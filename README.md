1. I am using Python 3.5.2, gcc/g++ 9.2.1.
2. Install PetSc https://www.mcs.anl.gov/petsc/documentation/installation.html
3. Install cnpy by following the instructions in its GitHub page https://github.com/rogersce/cnpy
4. Change PETSC_DIR and PETSC_ARCH variables in CMakeLists.txt according to your PetSc installation.
5. Compile C++ executable with CMake and gcc/g++. Make sure the name of the build directory is "cmake-build-debug"
6. Put the matrices (in Matrix Market format .mtx) you want to solve under the python directory (the same directory with GenerateRandomMatrix.py)
7. Run GenerateRandomMatrix.py from the same directory it exists(this is important), it will solve the matrices in that directory and writes output files (result data in csv format) under the cmake-build-debug directory.
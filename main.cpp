#include "petsc.h"
#include "petscksp.h"

#include "cnpy.h"
#include <string>
#include <vector>
#include <chrono>


int main(int argc,char **args)
{
    std::string file = args[1];
    //std::string file = "data0.npy";
    cnpy::NpyArray arr = cnpy::npy_load(file);
    int dataSize = arr.num_vals;
    float* dataVec = new float[dataSize];
    dataVec = arr.data<float>();

    std::string file2 = args[2];
    //std::string file2 = "indices0.npy";
    cnpy::NpyArray arr2 = cnpy::npy_load(file2);
    int indicesSize = arr2.num_vals;
    int* indicesVec = new int[indicesSize];
    indicesVec = arr2.data<int>();

    std::string file3 = args[3];
    //std::string file3 = "indptr0.npy";
    cnpy::NpyArray arr3 = cnpy::npy_load(file3);
    int indptrSize = arr3.num_vals;
    int* indptrVec = new int[indptrSize];
    indptrVec = arr3.data<int>();

    PetscInitialize( &argc, &args, 0, 0 );
    PetscErrorCode ierr;

    PetscScalar* dataPetsc = new PetscScalar[dataSize];
    for(std::size_t i = 0; i < dataSize; ++i)
        dataPetsc[i] = dataVec[i];

    PetscInt* indicesPetsc = new PetscInt[indicesSize];
    for(int i = 0; i < indicesSize; ++i)
        indicesPetsc[i] = indicesVec[i];

    PetscInt* indptrPetsc = new PetscInt[indptrSize];
    for(int i = 0; i < indptrSize; ++i)
        indptrPetsc[i] = indptrVec[i];

    const int N = indptrSize - 1;

    Mat mat;
    MatCreateSeqAIJWithArrays(PETSC_COMM_WORLD, N, N, indptrPetsc, indicesPetsc, dataPetsc, &mat);

    ierr = MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    Vec x,b;
    ierr = VecCreate(PETSC_COMM_WORLD, &x);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
    ierr = VecSetSizes(x, PETSC_DECIDE, N);
    ierr = VecSetFromOptions(x);CHKERRQ(ierr);
    ierr = VecDuplicate(x, &b);CHKERRQ(ierr);

    auto start = std::chrono::system_clock::now();

    /*
       Set exact solution; then compute right-hand-side vector.
    */
    Vec u;
    ierr = VecDuplicate(x, &u);CHKERRQ(ierr);
    VecSet(u,1.0);
    MatMult(mat,u,b);
    //PetscScalar p = 1;
    //ierr = VecSet(b, p);CHKERRQ(ierr);

    KSP ksp;
    ierr = KSPCreate(PETSC_COMM_WORLD, &ksp);CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp, mat, mat);CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp, 1.e-5, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);CHKERRQ(ierr);

    PC pc;
    ierr = PCCreate(PETSC_COMM_WORLD, &pc);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp, &pc);CHKERRQ(ierr);
    //ierr = PCSetFromOptions(pc);CHKERRQ(ierr);
    if(std::string(args[4]) == "lu") {
        ierr = PCSetType(pc, PCLU);
        CHKERRQ(ierr);
    }else if(std::string(args[4]) == "jacobi") {
        ierr = PCSetType(pc, PCJACOBI);
        CHKERRQ(ierr);
    }else if(std::string(args[4]) == "nopc"){
        ierr = PCSetType(pc, PCNONE);CHKERRQ(ierr);
    }


    ierr = KSPSolve(ksp, b, x);CHKERRQ(ierr);
    ierr = KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                       Check the solution and clean up
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    PetscReal norm;
    PetscInt its;
    VecAXPY(x,-1.0,u);
    VecNorm(x,NORM_2,&norm);
    KSPGetIterationNumber(ksp,&its);
    PetscPrintf(PETSC_COMM_WORLD,"Norm of error %g, Iterations %D\n",(double)norm,its);

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "Chrono Elapsed Time: " << elapsed_seconds.count() << std::endl;

    ierr = PetscFinalize();
    return ierr;
}

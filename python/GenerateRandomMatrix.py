import scipy.sparse as sparse
import numpy as np
import matplotlib.pyplot as plt
import os as os
from time import time
import csv
from scipy.io import mmread
import glob

file_list = glob.glob("*.mtx")
print(file_list)
file_list = [file_name for file_name in file_list if file_name.find("_b") == -1 and file_name.find("coord") == -1]
print(file_list)


#file_list = ["bcsstk14.mtx", "bcsstk35.mtx", "bcsstk38.mtx", "Chebyshev3.mtx", "delaunay_n19.mtx" \
#	     , "engine.mtx", "ford2.mtx", "Kuu.mtx", "Lin.mtx", "lock2232.mtx", "msc00726.mtx" \
#	     , "netherlands_osm.mtx", "opt1.mtx", "pkustk08.mtx", "rgg_n_2_18_s0.mtx", "s3dkt3m2.mtx" \
#             , "ship_001.mtx", "shipsec5.mtx", "sme3Da.mtx", "thread.mtx", "trdheim.mtx", "vanbody.mtx"]

file_list2 = ["bcsstk14.mtx", "bcsstk35.mtx", "bcsstk38.mtx", "Chebyshev3.mtx" \
	     , "Kuu.mtx", "msc00726.mtx" \
	     ,  "opt1.mtx", "pkustk08.mtx" \
             , "ship_001.mtx", "sme3Da.mtx", "thread.mtx", "trdheim.mtx"]

pc_type_list = ["lu","jacobi"]

for file_name in file_list:
	os.chdir("../python")
	A = mmread(file_name)
	A = sparse.csr_matrix(A)
	
#	upper_total_bandwidth = 0
#	upper_max_bandwidth = 0
#	upper_avg_bandwidth = 0
#	upper_number_of_values = 0
#	upper_size_bandwidth_ratio = 0
#	lower_total_bandwidth = 0
#	lower_max_bandwidth = 0
#	lower_avg_bandwidth = 0
#	lower_number_of_values = 0
#	lower_size_bandwidth_ratio = 0
#	cx = A.tocoo()
#	for i,j in zip(cx.row, cx.col):
#		if i > j:
#			bandwidth = i - j
#			lower_total_bandwidth = lower_total_bandwidth + bandwidth
#			lower_number_of_values += 1
#			if bandwidth > lower_max_bandwidth:
#				lower_max_bandwidth = bandwidth
#		elif i < j:
#			bandwidth = j - i
#			upper_total_bandwidth = upper_total_bandwidth + bandwidth
#			upper_number_of_values += 1
#			if bandwidth > upper_max_bandwidth:
#				upper_max_bandwidth = bandwidth
#	if lower_number_of_values > 0:
#		lower_avg_bandwidth = lower_total_bandwidth / lower_number_of_values
#		lower_size_bandwidth_ratio = A.shape[0] / lower_avg_bandwidth
#	if upper_number_of_values > 0:		
#		upper_avg_bandwidth = upper_total_bandwidth / upper_number_of_values
#		upper_size_bandwidth_ratio = A.shape[0] / upper_avg_bandwidth
#	
#	for _ in range(2):
#		bandwidth_file = open("bandwidth_30_04.txt", mode='a+')
#		file_writer = csv.writer(bandwidth_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
#		file_writer.writerow([file_name, lower_max_bandwidth, lower_avg_bandwidth, lower_size_bandwidth_ratio, upper_max_bandwidth, upper_avg_bandwidth, upper_size_bandwidth_ratio])
#		bandwidth_file.close()	
#
#	print(" L max: " + str(lower_max_bandwidth))
#	print(" L avg: " + str(lower_avg_bandwidth))
#	print(" L max: " + str(lower_max_bandwidth))
#	print(" L avg: " + str(lower_avg_bandwidth))

	os.chdir("../cmake-build-debug")
	path = "../cmake-build-debug/"
	indicesFile = "A_indices" + ".npy"
	indicesFileWithPath = path + indicesFile
	indptrFile = "A_indptr" + ".npy"
	indptrFileWithPath = path + indptrFile
	dataFile = "A_data" + ".npy"
	dataFileWithPath = path + dataFile
#		condNum = np.linalg.cond(A.toarray())
	matrixSize = A.shape[0]
	print("\n\nFile name: " + file_name)
#		print("File name: " + file_name + " Cond Num: " + str(condNum))
	np.save(indicesFileWithPath, A.indices)
#	print(A.indices)
	np.save(indptrFileWithPath, A.indptr)
#	print(A.indptr)
	np.save(dataFileWithPath, A.data)
#	print(A.data)
	for pcType in pc_type_list:
		for _ in range(1):		
			start_time = time();
			os.system("./PetscHelloWorld " + dataFile + " " + indicesFile + " " + indptrFile + " " + pcType + " -memory_view")
			end_time = time()
			solver_time = end_time - start_time
			numNZE = A.count_nonzero()
			print("Number of NZE: ", numNZE)
			print("Time: ", solver_time)
		
			if pcType == "jacobi" or pcType == "lu":
				pcTypeID = [1 if pcType == "jacobi" else 0][0]
				results_file = open("results_direct_28_08.txt", mode='a+')
				result_writer = csv.writer(results_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
				result_writer.writerow([file_name, matrixSize, numNZE, solver_time])
				results_file.close()
			
			if pcType == "jacobi" or pcType == "nopc":
				pcTypeID = [1 if pcType == "jacobi" else 0][0]
				results_file2 = open("results_pc_vs_nopc_30_04.txt", mode='a+') 
				result_writer = csv.writer(results_file2, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
				result_writer.writerow([file_name, matrixSize, numNZE, pcTypeID, solver_time])
				results_file2.close()



#for matrixSize in range(1000, 2001, 1000):
#
#	for density in range(1, 2):
#
#		density = density * 0.025
#
#		for condScale in range(50, 101, 50):
#
#			A = 10 * sparse.rand(matrixSize, matrixSize, density = density, format = 'csr', dtype=np.float32)
#			B = condScale * sparse.identity(matrixSize, format = 'csr', dtype=np.float32)
#			A = A + B
#			path = "../cmake-build-debug/"
#			indicesFile = "A_demo_indices" + str(matrixIndex) + ".npy"
#			indicesFileWithPath = path + indicesFile
#			indptrFile = "A_demo_indptr" + str(matrixIndex) + ".npy"
#			indptrFileWithPath = path + indptrFile
#			dataFile = "A_demo_data" + str(matrixIndex) + ".npy"
#			dataFileWithPath = path + dataFile
#			np.save(indicesFileWithPath, A.indices)
#			np.save(indptrFileWithPath, A.indptr)
#			np.save(dataFileWithPath, A.data)
#			start_time = time();
#			condNum = np.linalg.cond(A.toarray())
#			print("Cond Number: ", condNum)
#			end_time = time()
#			elapsed_time = end_time - start_time
#			print("Cond Number Calculation Time: ", elapsed_time)
#			#plt.spy(A, markersize=0.1)
#			#plt.show(block=True)
#			matrixIndex = matrixIndex + 1
#
#			for pcTypeIndex in range(1, 4):
#				if pcTypeIndex == 1:
#					pcType = "jacobi"
#				elif pcTypeIndex == 2:
#					pcType = "sor"
#				elif pcTypeIndex == 3:
#					pcType = "lu"
#
#				start_time = time();
##				os.chdir("../cmake-build-debug")
#				os.system("./PetscHelloWorld " + dataFile + " " + indicesFile + " " + indptrFile + " " + pcType)
#				end_time = time()
#				solver_time = end_time - start_time
#				numNZE = A.count_nonzero()
#				print("Number of NZE: ", numNZE)
#				print("Time: ", solver_time)
#
##				os.chdir("../cmake-build-debug")
#				result_writer = csv.writer(results_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
#				result_writer.writerow([matrixIndex, matrixSize, numNZE, condNum, pcTypeIndex, solver_time])
#


